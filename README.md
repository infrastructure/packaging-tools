Packaging tools
===============

This repository is home for a bunch of assorted Apertis packaging tools.

Runtime dependencies:
 * Python 3.7 or later
 * git-buildpackage
 * python3-sh
 * python3-debian
 * pristine-lfs

* `checkout-tarball`: inspects the currently checked out packaging branch
  and checks out matching tarballs from the `pristine-lfs` branch.

  Obsoleted by `pristine-lfs checkout --auto`

* `import-tarballs`: given a `.dsc` file, imports the original tarballs
  (the main `.orig.tar.*` and the components `.orig-*.tar.*`) into the
  Git LFS storage, storing the metadata on the `pristine-lfs` branch.

  Obsoleted by `pristine-lfs import-dsc`
